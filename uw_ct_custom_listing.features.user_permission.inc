<?php

/**
 * @file
 * uw_ct_custom_listing.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_custom_listing_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uwaterloo_custom_listing content'.
  $permissions['create uwaterloo_custom_listing content'] = array(
    'name' => 'create uwaterloo_custom_listing content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uwaterloo_custom_listing content'.
  $permissions['delete any uwaterloo_custom_listing content'] = array(
    'name' => 'delete any uwaterloo_custom_listing content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uwaterloo_custom_listing content'.
  $permissions['delete own uwaterloo_custom_listing content'] = array(
    'name' => 'delete own uwaterloo_custom_listing content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uwaterloo_custom_listing content'.
  $permissions['edit any uwaterloo_custom_listing content'] = array(
    'name' => 'edit any uwaterloo_custom_listing content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uwaterloo_custom_listing content'.
  $permissions['edit own uwaterloo_custom_listing content'] = array(
    'name' => 'edit own uwaterloo_custom_listing content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uwaterloo_custom_listing revision log entry'.
  $permissions['enter uwaterloo_custom_listing revision log entry'] = array(
    'name' => 'enter uwaterloo_custom_listing revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission:
  // 'override uwaterloo_custom_listing authored by option'.
  $permissions['override uwaterloo_custom_listing authored by option'] = array(
    'name' => 'override uwaterloo_custom_listing authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission:
  // 'override uwaterloo_custom_listing authored on option'.
  $permissions['override uwaterloo_custom_listing authored on option'] = array(
    'name' => 'override uwaterloo_custom_listing authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission:
  // 'override uwaterloo_custom_listing promote to front page option'.
  $permissions['override uwaterloo_custom_listing promote to front page option'] = array(
    'name' => 'override uwaterloo_custom_listing promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uwaterloo_custom_listing published option'.
  $permissions['override uwaterloo_custom_listing published option'] = array(
    'name' => 'override uwaterloo_custom_listing published option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'content editor' => 'content editor',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uwaterloo_custom_listing revision option'.
  $permissions['override uwaterloo_custom_listing revision option'] = array(
    'name' => 'override uwaterloo_custom_listing revision option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uwaterloo_custom_listing sticky option'.
  $permissions['override uwaterloo_custom_listing sticky option'] = array(
    'name' => 'override uwaterloo_custom_listing sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
